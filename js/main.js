$(document).ready(function(){
    $("#main_slider").owlCarousel({
        loop:false,
        margin:10,
        items:1,
        nav: true,
        dots: true,
        mouseDrag: false,
        navText : ["",""],
        responsive:{
            0:{
                items:1
            },
            400:{
                items:1
            },
            500:{
                items:1
            },
            700:{
                items:1
            },
            1023:{
                items:1,
                mouseDrag: false
            },
            1200:{
                items:1,
                mouseDrag: true
            }

        }
    });

    $("#history-owl").owlCarousel({
        loop:false,
        margin:10,
        items:1,
        nav: true,
        dots: true,
        mouseDrag: false,
        navText : ["",""],
        responsive:{
            0:{
                items:1
            },
            400:{
                items:1
            },
            500:{
                items:1
            },
            700:{
                items:1
            },
            1023:{
                items:1,
                mouseDrag: false
            },
            1200:{
                items:1,
                mouseDrag: true
            }

        }
    });

    $("#best_sellers").owlCarousel({
        loop:false,
        margin:10,
        items:1,
        nav: true,
        dots: false,
        mouseDrag: false,
        navText : ["",""],
        responsive:{
            1200:{
                items:6,
                mouseDrag: true
            }

        }
    });

  });
